package com.example.out2.DTO;

import com.example.out2.entity.Category;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

//@AllArgsConstructor
@Data
public class CategoryChildrenViewDTO {
    private UUID id;
    private String name;
    private CategoryParentViewDTO parent;

    public CategoryChildrenViewDTO(UUID id, String name, Category category) {
        this.id = id;
        this.name = name;
        CategoryParentViewDTO parent = new CategoryParentViewDTO(category.getCategory_id(), category.getName());
        this.parent = parent;
    }
}
