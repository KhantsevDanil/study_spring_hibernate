package com.example.out2.DTO;

import lombok.Data;

import java.util.List;
import java.util.UUID;


@Data
public class CategoryCreateChildrenDTO {
    private UUID parent_id;
    private List<String> name;
}
