package com.example.out2.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@Data
public class CategoryCreateDTO {
    private String name;
    private UUID parent_id;
}
