package com.example.out2.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@AllArgsConstructor
@Data
public class CategoryParentViewDTO {
    private UUID id;
    private String name;
}
