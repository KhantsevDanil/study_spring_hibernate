package com.example.out2.DTO;

import com.example.out2.entity.Category;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Set;
import java.util.UUID;
@AllArgsConstructor
@Data
public class CategoryViewDTO {
    UUID id;
    String name;
    Category parent;
}
