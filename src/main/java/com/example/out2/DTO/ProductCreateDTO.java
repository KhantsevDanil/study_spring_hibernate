package com.example.out2.DTO;

import com.example.out2.entity.Category;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.UUID;

@Data
public class ProductCreateDTO {
    private UUID id;
    private String name;

    private String description;
    private int price;

    private UUID category_id;
}
