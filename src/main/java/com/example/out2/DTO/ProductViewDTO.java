package com.example.out2.DTO;

import com.example.out2.entity.Category;
import com.example.out2.entity.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

@AllArgsConstructor
@Data
public final class ProductViewDTO
{
    private final UUID id;
    private final String name;
    private final String description;
    private final int price;
    private final String category_name;
    public static @NotNull ProductViewDTO of(@NotNull Product product){
        return new ProductViewDTO(product.getId(),product.getName(), product.getDescription(), product.getPrice(), product.getCategory().getName());
    }
}