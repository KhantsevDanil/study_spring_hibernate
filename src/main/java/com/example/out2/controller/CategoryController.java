package com.example.out2.controller;

import com.example.out2.DTO.CategoryCreateChildrenDTO;
import com.example.out2.DTO.CategoryChildrenViewDTO;
import com.example.out2.DTO.CategoryCreateDTO;
import com.example.out2.DTO.CategoryViewDTO;
import com.example.out2.entity.Category;
import com.example.out2.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RequiredArgsConstructor
@RestController
@RequestMapping("/category")
public class CategoryController {

    private final CategoryService categoryService;
    @PostMapping("/children")
    public List<CategoryChildrenViewDTO> createChildrenForOneCategory(@RequestBody CategoryCreateChildrenDTO categoryCreateDTO){
        return  categoryService.createCategoryChildren(categoryCreateDTO);
    }
    @GetMapping
    public List<Category> getCategory(){
        return categoryService.getLazyCategory();
    }
    @PostMapping
    public CategoryViewDTO createCategory(@RequestBody CategoryCreateDTO categoryCreateDTO){
        return categoryService.save(categoryCreateDTO);
    }
}
