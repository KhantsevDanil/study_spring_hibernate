package com.example.out2.controller;


import com.example.out2.DTO.ProductCreateDTO;
import com.example.out2.DTO.ProductViewDTO;
import com.example.out2.entity.Category;
import com.example.out2.entity.Product;
import com.example.out2.service.CategoryService;
import com.example.out2.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/product")
public class ProductController {

    private final ProductService productService;
    @GetMapping("")
    public List<Product> getAll(){
        return productService.getAll();
    }
    @PostMapping("")
    public ProductViewDTO createPost(@RequestBody ProductCreateDTO productCreateDTO){
        ProductViewDTO result = (ProductViewDTO) productService.createPost(productCreateDTO);
        return result;
    }
}
