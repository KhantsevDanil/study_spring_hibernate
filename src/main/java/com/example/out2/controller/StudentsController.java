package com.example.out2.controller;


import com.example.out2.entity.Student;
import com.example.out2.service.StudentService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/students")
public class StudentsController {

    private final StudentService studentService;

    //TODO use lombok annotation to generate constructor
    public StudentsController(StudentService studentService) {
        this.studentService = studentService;
    }
    @GetMapping(value="/{id}")
    public Student getStudentById(@PathVariable("id") @Min(1) int id) {
        Optional<Student> std = studentService.findById(id);
        return std.get();
    }

    @PostMapping()
    public Student createStudent(@Valid @RequestBody Student std){
        return studentService.save(std);
    }
    @PutMapping(value="/{id}")
    public Student updateStudent(@PathVariable("id") @Min(1) int id, @Valid @RequestBody Student newstd) {
        Optional<Student> stdu = studentService.findById(id);
        stdu.get().setFirstname(newstd.getFirstname());
        stdu.get().setLastname(newstd.getLastname());
        return studentService.save(stdu.get());
    }
    @DeleteMapping(value="/{id}")
    public String deleteStudent(@PathVariable("id") @Min(1) int id) {
        Optional<Student> std = studentService.findById(id);
        studentService.deleteById(std.get().getId());
        return "Student with ID :"+id+" is deleted";
    }
    @GetMapping
    public List<Student> getAllStudents() {
        List<Student> std = studentService.getStudents();
        return std;
    }

}
