package com.example.out2.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.*;


// Почему тут нельзя поставить @AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="category", schema = "public")
@Data
public class Category {
    
    @GeneratedValue(generator = "uuid4")
    @GenericGenerator(name = "UUID", strategy = "uuid4")
    private @Id @Setter(AccessLevel.PROTECTED) UUID category_id;

    private String name;

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id", nullable = true)
    private Category category;

    public Category(String name, Category category) {
        this.name = name;
        this.category = category;
    }

//    public Category(String name, Category category){
//        this.name=name;
//        this.category=category;
//    }
}
