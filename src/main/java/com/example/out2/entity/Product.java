package com.example.out2.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@AllArgsConstructor
@Entity
@Table(name="product", schema = "public")
@Data
public class Product {
    @GeneratedValue(generator = "uuid4")
    @GenericGenerator(name = "UUID", strategy = "uuid4")
    private @Id @Setter(AccessLevel.PROTECTED) UUID id;

    private String name;
    private String description;
    private int price;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "category_id", nullable = true)
    private Category category;

    public Product() {

    }
//    public Product(UUID id,String name, String description, int price, Category category) {
//        this.id=id;
//        this.name = name;
//        this.description = description;
//        this.category=category;
//        this.price=price;
//    }
}
