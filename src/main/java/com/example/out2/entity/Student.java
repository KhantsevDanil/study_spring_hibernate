package com.example.out2.entity;

import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;


// Почему тут нельзя поставить @AllArgsConstructor


@Entity
@Table(name="student", schema = "public")
@Data
//@Accessors(chain=true)
@NoArgsConstructor
public class Student {
    private @Id @Setter(AccessLevel.PROTECTED) int id;
    int age;

    @NotEmpty(message = "First name is required")
    private String firstname;

    @NotEmpty(message = "Last name is required")
    private String lastname;

    int tiredness;
}
