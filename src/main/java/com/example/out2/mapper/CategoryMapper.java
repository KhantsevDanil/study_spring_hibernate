package com.example.out2.mapper;

import com.example.out2.DTO.CategoryChildrenViewDTO;
import com.example.out2.entity.Category;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CategoryMapper {

    CategoryMapper INSTANCE = Mappers.getMapper(CategoryMapper.class);
    @Mapping(source = "category.category", target = "parent")
    CategoryChildrenViewDTO toDto(Category category);
}