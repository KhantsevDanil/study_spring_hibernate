package com.example.out2.service;

import com.example.out2.DTO.CategoryCreateChildrenDTO;
import com.example.out2.DTO.CategoryChildrenViewDTO;
import com.example.out2.DTO.CategoryCreateDTO;
import com.example.out2.DTO.CategoryViewDTO;
import com.example.out2.entity.Category;
import com.example.out2.mapper.CategoryMapper;
import com.example.out2.repository.CategoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.reactive.TransactionalOperator;

import javax.transaction.Transactional;
import java.util.*;

@RequiredArgsConstructor
@Service
public class CategoryService {
    private final CategoryRepository categoryRepository;

    @Transactional
    public List<CategoryChildrenViewDTO> createCategoryChildren(CategoryCreateChildrenDTO categoryCreateDTO) {
        Category mainCategory = categoryRepository.getById(categoryCreateDTO.getParent_id()); // напиши орбработчик исключения, если нет родительской категории
        List<String> names = categoryCreateDTO.getName();
        List<CategoryChildrenViewDTO> categories = new ArrayList<>();
        for (String name : names) {
            Category category = new Category(name, mainCategory);
            categoryRepository.save(category);
            categories.add(CategoryMapper.INSTANCE.toDto(category));
        }
        return categories;
    }

    public CategoryViewDTO save(CategoryCreateDTO categoryCreateDTO) {
        if (categoryCreateDTO.getParent_id() != null) {
            Category mainCategory = categoryRepository.getById(categoryCreateDTO.getParent_id());
            Category category = categoryRepository.save(new Category(categoryCreateDTO.getName(), mainCategory));
            return new CategoryViewDTO(category.getCategory_id(), category.getName(), category.getCategory());
        } else {
            Category category = categoryRepository.save(new Category(categoryCreateDTO.getName(), null));
            return new CategoryViewDTO(category.getCategory_id(), category.getName(), category.getCategory());
        }
    }

    public List<Category> getLazyCategory() {
        return categoryRepository.findAll();
    }
}

