package com.example.out2.service;

import com.example.out2.DTO.ProductCreateDTO;
import com.example.out2.DTO.ProductViewDTO;
import com.example.out2.entity.Category;
import com.example.out2.entity.Product;
import com.example.out2.repository.CategoryRepository;
import com.example.out2.repository.ProductRepository;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class ProductService {
    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;
    public ProductViewDTO createPost(ProductCreateDTO productCreateDTO) {
        Category category= categoryRepository.getById(productCreateDTO.getCategory_id());
        Product product=productRepository.save(new Product(
                productCreateDTO.getId(),
                productCreateDTO.getName(),
                productCreateDTO.getDescription(),
                productCreateDTO.getPrice(),
                category));
        return ProductViewDTO.of(product);
    }
    public List<Product> getAll(){
        return productRepository.findAll();
    }
}
