package com.example.out2.service;

import com.example.out2.entity.Category;
import com.example.out2.entity.Student;
import com.example.out2.repository.StudentRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentService {
    private final StudentRepository studentRepository;

    public StudentService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }
    public Student save(Student std) {
        return studentRepository.save(std);
    }
    public void deleteById(int id) {
        studentRepository.deleteById(id);
    }

    public Optional<Student> findById(Integer id) {

        return studentRepository.findById(id);
    }

//    public Category reducer(Category first, Category child) {
//
////        if (child.getParent() == parent.getId()) {
////            parent.getChild().add(child);
////        } else {
////            parent.getChild().forEach(parent -> reducer(parent, child));
////        }
//    }
    public List<Student> getStudents(){
         return studentRepository.findAll();
    }

//
//    public Student getStudent(Integer studentId){
//      return studentRepository.findById(studentId).get();
//  }
}
